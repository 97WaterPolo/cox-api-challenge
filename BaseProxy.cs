﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoxApi
{
    class BaseProxy
    {
        private HttpClient _client;
        public BaseProxy()
        {
            _client = new HttpClient();
        }

        public async Task<T> GetAsync<T>(string rootUrl)
        {
            var response = await _client.GetAsync(rootUrl);
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());

        }

        public async Task<T> PostAsync<T,U>(string rootUrl, U data)
        {
            var jsonPayload = JsonConvert.SerializeObject(data);

            var response = await _client.PostAsync(rootUrl, PrepJsonForPost(jsonPayload));

            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());

        }

        private StringContent PrepJsonForPost(string jsonObj)
        {
            return new StringContent(jsonObj, Encoding.UTF8, "application/json");
        }
    }
}
