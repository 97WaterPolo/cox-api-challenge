﻿using CoxApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoxApi
{
    class CoxService : BaseProxy
    {
        private readonly string BaseUrl = "https://api.coxauto-interview.com";
        private readonly string GetApiDatasetId = "/api/datasetId";
        private readonly string PostAnswer = "/api/{0}/answer";
        private readonly string GetDealers = "/api/{0}/dealers/{1}";
        private readonly string GetVehicles = "/api/{0}/vehicles";
        private readonly string GetSpecificVehicle= "/api/{0}/vehicles/{1}";

        public async Task<AnswerResponse> GenerateCoxAutoCatalog()
        {
            //Create a new dataset object
            var dataSetIdResponse = await GetAsync<DataSetIdResponse>(BaseUrl + GetApiDatasetId);

            //Obtain all of the vehicles
            var vehiclesResponse = await GetAsync<VehicleIdsResponse>(BaseUrl + string.Format(GetVehicles, dataSetIdResponse.DatasetId));
            var vehicles = await GetAllObject<VehicleResponse>(GetSpecificVehicle, dataSetIdResponse.DatasetId, vehiclesResponse.VehicleIds);//await GetAllVehiclesInfo(dataSetIdResponse.DatasetId, vehiclesResponse.VehicleIds);

            //Obtain all the dealers
            var dealerIds = vehicles.Select(x => x.DealerId);
            var dealers = await GetAllObject<DealerResponse>(GetDealers, dataSetIdResponse.DatasetId, dealerIds);//await GetAllDealers(dataSetIdResponse.DatasetId, dealerIds);

            //Mapping all the objects above to the correct Answer format
            var aDealers = dealers.GroupBy(x => x.DealerId).Select(x => x.First()).Select(dealer => new Dealer()
            {
                DealerId = dealer.DealerId,
                Name = dealer.Name,
                Vehicles = vehicles.Where(x => x.DealerId.Equals(dealer.DealerId)).Select(x => new Vehicle()
                {
                    Make = x.Make,
                    Model = x.model,
                    VehicleId = x.VehicleId,
                    Year = x.Year
                })
            });

            //Post back our answer and see if it is correct!
            return await PostAsync<AnswerResponse, AnswerRequest>(BaseUrl + string.Format(PostAnswer, dataSetIdResponse.DatasetId), new AnswerRequest() { Dealers = aDealers });
        }

        private async Task<IEnumerable<T>> GetAllObject<T>(string endpoint, string datasetId, IEnumerable<int> ids)
        {
            var tasks = new List<Task<T>>();
            foreach (var id in ids)
            {
                tasks.Add(GetAsync<T>(BaseUrl + string.Format(endpoint, datasetId, id)));
            }
            return await Task.WhenAll(tasks);
        }

    }
}
