﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoxApi.Models
{
    class DataSetIdResponse
    {
        [JsonProperty("datasetId")]
        public string DatasetId { get; set; }
    }
}
