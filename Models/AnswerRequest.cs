﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoxApi.Models
{
    class AnswerRequest
    {
        [JsonProperty("dealers")]
        public IEnumerable<Dealer> Dealers { get; set; }
    }

    class Dealer
    {
        [JsonProperty("dealerId")]
        public int DealerId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("vehicles")]
        public IEnumerable<Vehicle> Vehicles { get; set; }
    }

    class Vehicle
    {
        [JsonProperty("vehicleId")]
        public int VehicleId { get; set; }
        [JsonProperty("year")]
        public int Year { get; set; }
        [JsonProperty("make")]
        public string Make { get; set; }
        [JsonProperty("model")]
        public string Model { get; set; }
    }

}
