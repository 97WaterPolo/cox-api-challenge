﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoxApi.Models
{
    class DealerResponse
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("dealerId")]
        public int DealerId { get; set; }
    }
}
