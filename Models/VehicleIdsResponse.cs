﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoxApi.Models
{
    class VehicleIdsResponse
    {
        [JsonProperty("vehicleIds")]
        public int[] VehicleIds { get; set; }
    }
}
