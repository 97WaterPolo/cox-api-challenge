﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoxApi
{
    class Program
    {
        static void Main(string[] args)
        {
            var answer = new CoxService().GenerateCoxAutoCatalog().Result;
            Console.WriteLine(string.Format("Did you succeed? {0}", answer.Success));
            Console.WriteLine(string.Format("The server told us {0}", answer.Message));
            Console.WriteLine(string.Format("Total time is {0} seconds", (answer.TotalMillis/1000.0)));
            Console.WriteLine();
        }
    }
}
